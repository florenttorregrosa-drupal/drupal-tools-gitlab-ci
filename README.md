# Drupal tools: Gitlab CI

Provides common jobs for Drupal projects. So CI configuration maintenance for
those projects is eased.

Initial code quality and tests jobs taken from
[Docker Drupal Project](https://gitlab.com/florenttorregrosa-drupal/docker-drupal-project).


## Acknowledgment

Inspiration taken from the following repositories:
- https://gitlab.com/mog33/gitlab-ci-drupal
- https://git.drupalcode.org/project/gitlab_templates/
- https://git.drupalcode.org/sandbox/liberT-3399709
